import { Component, OnInit, HostListener, } from '@angular/core';



@Component({
  selector: 'bord',
  templateUrl: './bord.component.html',
  styleUrls: ['./bord.component.scss']
})



export class BordComponent implements OnInit {

  score: number;
  bestScore: number;

  ngOnInit() {
    this.newGame();
    this.bestScore = JSON.parse(localStorage.getItem("bestScore"))
  }
  constructor() {
  }

  @HostListener('document: keydown.R', ['$event.target']) R() {
    event.preventDefault();
    this.newGame();

  }

  @HostListener('document: keydown.ArrowDown', ['$event.target'])
  arrowDown() {
    const oldElements = this.elements.map(element => { return element });
    console.log('Moving Down')
    this.moveDown();
    this.checkElements(oldElements);
  };
  @HostListener('document: keydown.ArrowLeft', ['$event.target'])
  arrowLeft() {
    const oldElements = this.elements.map(element => { return element });
    console.log('Moving Left');
    this.moveLeft();
    this.checkElements(oldElements);

  };
  @HostListener('document: keydown.ArrowRight', ['$event.target'])
  arrowRight() {
    const oldElements = this.elements.map(element => { return element });
    console.log('Moving Right');
    this.moveRight();
    this.checkElements(oldElements);
  };
  @HostListener('document: keydown.ArrowUp', ['$event.target'])
  arrowUp() {
    const oldElements = this.elements.map(element => { return element });
    console.log('Move up')
    this.moveUp();
    this.checkElements(oldElements);
  };

  elements = new Array(16).fill(0);

  mergedTiles = [];

  newGame() {
    this.elements = new Array(16).fill(0);
    this.setRandomPile(true);
    this.score = 0;

  }

  getRandomNumber() {
    return Math.random() < 0.9 ? 2 : 4;
  }

  getRandomPile() {
    return Math.floor(Math.random() * (Math.floor(15) - Math.ceil(0))) + Math.ceil(0);
  }
  setRandomPile(isNewGame: boolean) {
    for (let i = 0; i < (isNewGame ? 2 : 1); i++) {
      let index = this.getRandomPile();
      if (this.elements[index] == 0) {
        this.elements[index] = this.getRandomNumber()
      }
    }
    return this.elements;
  }

  checkElements(oldElements) {
    for (let i = 0; i < 15; i++)
      if (oldElements[i] !== this.elements[i]) {
        this.setRandomPile(false);
        break;
      }
  }


  localStorage() {
    localStorage.setItem('bestScore', JSON.stringify(this.bestScore));
  }



  actionUp() {
    for (let i = 15; i >= 0; i--) {
      for (let j = 15; j >= 0; j--) {
        if (this.elements[j] !== 0 && (this.elements[j] === this.elements[j - 4] || this.elements[j - 4] === 0)) {
          this.elements[j - 4] += this.elements[j];
          this.elements[j] = 0;
        }
      }
    }
    return this.elements;
  }

  moveUp() {
    this.actionUp();
    return this.elements;
  }



  actionDown() {
    for (let i = 0; i <= 15; i++) {
      for (let j = 0; j <= 15; j++) {
        if (this.elements[j] !== 0 && ((this.elements[j] === this.elements[j + 4] || this.elements[j + 4] === 0))) {
          this.elements[j + 4] += this.elements[j];
          this.elements[j] = 0;
        }
      }
    }
    return this.elements;
  }

  moveDown() {
    this.actionDown();
    return this.elements;
  }

  moveLeft() {
    for (let i = 0; i <= 15; i++) {
      for (let j = 0; j <= 15; j++) {
        if ((j === 0) || (j === 4) || (j === 8) || (j === 12)) { continue; }
        if (this.elements[j] !== 0 && (this.elements[j] === this.elements[j - 1] || this.elements[j - 1] === 0)) {
          this.elements[j - 1] += this.elements[j];
          this.elements[j] = 0;
        }
      }
    }
    return this.elements;
  }


  moveRight() {
    for (let i = 0; i <= 15; i++) {
      for (let j = 0; j <= 15; j++) {
        if ((j === 3) || (j === 7) || (j === 11) || (j === 15)) { continue; }
        if (this.elements[j] !== 0 && (this.elements[j] === this.elements[j + 1] || this.elements[j + 1] === 0)) {
          this.elements[j + 1] += this.elements[j];
          this.elements[j] = 0;
        }
      }
    }
    return this.elements;
  }

}